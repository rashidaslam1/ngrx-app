import { decrement,increment } from './../../store/counter.action';
import { State } from './../../store/index';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { countSelector } from 'src/app/store/counter.selecter';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
})
export class CounterComponent implements OnInit {

  count$: Observable<number>;
  constructor(private store: Store<State>) { }

  ngOnInit() {
    this.count$ = this.store.pipe(select(countSelector));
    console.log(this.count$)
  }

  increment(value: number) {
    this.store.dispatch(increment());
  }

  decrement(value: number) {
    this.store.dispatch(decrement());
  }
}
