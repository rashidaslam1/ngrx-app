import { CounterState } from './counter.state';
import { createSelector, createFeatureSelector } from '@ngrx/store';


const appCounter = createFeatureSelector<CounterState>('counter');


export const countSelector = createSelector(
    appCounter,
    counter => counter.count
)
