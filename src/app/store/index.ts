import { CounterState } from './counter.state';
import { ActionReducerMap } from '@ngrx/store';
import { reducer as counterReducer } from "../store/counter.reducer";

export interface State {
    counter: CounterState
}

export const reducers: ActionReducerMap<State> = {
    counter: counterReducer
}