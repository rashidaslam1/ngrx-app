import { CounterState } from './counter.state';
import { createReducer, on, Action } from '@ngrx/store'
import { decrement, increment } from "./counter.action";

export const initial_state: CounterState = {
    count: 0
}

export const counterReducer = createReducer(initial_state,
    on(increment, state => {
        return {
            ...state,
            count: state.count + 1
        }
    }),
    on(decrement, state => {
        return {
            ...state,
            count: state.count - 1
        }
    })
)


export function reducer(state: CounterState | undefined, action: Action) {
    return counterReducer(state, action)
}