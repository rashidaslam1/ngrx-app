import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CounterComponent } from './components/counter/counter.component';
import { StoreModule } from '@ngrx/store';
import { reducers} from '../app/store/index'

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
  ],
  imports: [
    StoreModule.forRoot(reducers),
    BrowserModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
